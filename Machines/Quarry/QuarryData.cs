﻿using System;
using Terraria;
using Terraria.Graphics.Effects;
using Terraria.Graphics.Shaders;
using Terraria.ID;
using Terraria.ModLoader;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using Terraria.UI;
using Terraria.DataStructures;
using Terraria.GameContent.UI;
using Terraria.ObjectData;
using System.Runtime.Serialization;
using Terraria.ModLoader.IO;

namespace JojoleproTestMod.Machines.Quarry
{
	
	public class QuarryData
	{
		public Point16 mineTarget = new Point16(0,0);
		public QuarryData()
		{

		}
		public QuarryData (Point16 mineTarget)
		{
			this.mineTarget = mineTarget;
		}
		public override string ToString ()
		{
			return "Quarry[target="+mineTarget.ToString()+"]";
		}
	}
}

