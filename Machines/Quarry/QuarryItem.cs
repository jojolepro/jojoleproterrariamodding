﻿using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;
using Terraria.ID;

namespace JojoleproTestMod.Machines.Quarry
{
	public class QuarryItem : ModItem
	{
		public override void SetDefaults()
		{
			item.name = "Quarry";
			item.toolTip = "Mines chunks of the world.";
			item.width = 40;
			item.height = 40;
			item.useStyle = 1;
			item.maxStack = 99;
			item.rare = 12;
			item.useTurn = true;
			item.autoReuse = true;
			item.useAnimation = 15;
			item.useTime = 10;
			item.value = 10000;
			item.placeStyle = 0;
			item.consumable = true;
			item.createTile = mod.TileType ("QuarryTile");
		}

		public override void AddRecipes()
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.DirtBlock,1);
			recipe.AddTile(TileID.WorkBenches);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}

		/*public override void MeleeEffects(Player player, Rectangle hitbox)
		{
			int dust = Dust.NewDust(new Vector2(hitbox.X, hitbox.Y), hitbox.Width, hitbox.Height, mod.DustType("Sparkle"));
		}*/
	}
}