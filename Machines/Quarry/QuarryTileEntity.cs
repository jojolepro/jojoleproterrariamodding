﻿using System;
using Terraria;
using Terraria.Graphics.Effects;
using Terraria.Graphics.Shaders;
using Terraria.ID;
using Terraria.ModLoader;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using Terraria.UI;
using Terraria.DataStructures;
using Terraria.GameContent.UI;
using Terraria.ObjectData;
using Terraria.ModLoader.IO;

namespace JojoleproTestMod.Machines.Quarry
{
	public class QuarryTileEntity : DataTileEntity<QuarryData>
	{
		public override void Update ()
		{
			base.Update ();
			var r = instances [Position];
			if (r.mineTarget.Y < Main.ActiveWorldFileData.WorldSizeY)
			{
				//Main.tile[r.mineTarget.X,r.mineTarget.Y]
				WorldGen.KillTile(r.mineTarget.X,r.mineTarget.Y);
				r.mineTarget = new Point16 (r.mineTarget.X, r.mineTarget.Y + 1);
			}
		}
		public override int Hook_AfterPlacement (int i, int j, int type, int style, int direction)
		{
			var res = base.Hook_AfterPlacement (i, j, type, style, direction);
			var p = new Point16 (i, j);
			instances [p].mineTarget = new Point16 (i, j + 1);
			return res;
		}

	}
}

