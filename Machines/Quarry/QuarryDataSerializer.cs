﻿using System.Collections;
using Terraria.ModLoader.IO;
using Terraria.DataStructures;

namespace JojoleproTestMod.Machines.Quarry
{
	public class QuarryDataSerializer : TagSerializer<QuarryData, TagCompound>
	{
		public override TagCompound Serialize(QuarryData value) => new TagCompound
		{
			["mineTarget"] = value.mineTarget
		};

		public override QuarryData Deserialize(TagCompound tag)
		{
			var mineTarget = tag.Get<Point16> ("mineTarget");
			return new QuarryData(mineTarget);
		}
	}
}

