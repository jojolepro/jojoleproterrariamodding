﻿using System;
using Terraria;
using Terraria.Graphics.Effects;
using Terraria.Graphics.Shaders;
using Terraria.ID;
using Terraria.ModLoader;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using Terraria.UI;
using Terraria.DataStructures;
using Terraria.GameContent.UI;
using Terraria.ObjectData;
using System.Runtime.Serialization;
using Terraria.ModLoader.IO;

namespace JojoleproTestMod.Machines.TestMachine
{
	
	public class TestMachineData// : TagSerializable
	{

		//public new static Func<TagCompound, TestItem> DESERIALIZER = tag => new TestItem(new Point16(tag.GetInt("position.X"),tag.GetInt("position.Y")),tag.GetInt("stuff"));

		public int stuff;
		public TestMachineData()
		{

		}
		public TestMachineData (int stuff)
		{
			this.stuff = stuff;
		}
		/*protected TestItem(SerializationInfo info, StreamingContext context)
		{
			info.Get
		}*/
		//public TagCompound SerializeData() => new TagCompound { { "position.X", position.X },{"position.Y",position.Y},{"stuff",stuff}};
		public override string ToString ()
		{
			return "TestItem[stuff="+stuff+"]";
		}
	}
}

