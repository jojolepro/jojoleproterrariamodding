﻿using System;
using Terraria;
using Terraria.Graphics.Effects;
using Terraria.Graphics.Shaders;
using Terraria.ID;
using Terraria.ModLoader;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using Terraria.UI;
using Terraria.DataStructures;
using Terraria.GameContent.UI;
using Terraria.ObjectData;

namespace JojoleproTestMod.Machines.TestMachine
{
	public class TestMachineTile : ModTile
	{
		public override void SetDefaults ()
		{
			Main.tileSolid [Type] = true;
			Main.tileNoAttach [Type] = true;
			Main.tileFrameImportant [Type] = true;
			this.mineResist = 1;
			this.minPick = 1;
			this.drop = mod.ItemType("TestMachineItem");
			disableSmartCursor = true;
			TileObjectData.newTile.CopyFrom(TileObjectData.Style1x1);
			TileObjectData.newTile.HookPostPlaceMyPlayer = new PlacementHook(mod.GetTileEntity("TestMachineTileEntity").Hook_AfterPlacement, -1, 0, false);
			TileObjectData.addTile (Type);
			AddMapEntry (new Color (200, 200, 200));
		}
		public override void KillTile (int i, int j, ref bool fail, ref bool effectOnly, ref bool noItem)
		{
			if(!fail)
				mod.GetTileEntity ("TestMachineTileEntity").Kill(i,j);
		}
		public override void RightClick (int i, int j)
		{
			TestMachineTileEntity.instances [new Point16 (i, j)].stuff += 1;
			Main.NewText (Convert.ToString (TestMachineTileEntity.instances [new Point16 (i, j)].stuff));
		}
	}
}

