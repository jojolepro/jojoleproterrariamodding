﻿using System.Collections;
using Terraria.ModLoader.IO;

namespace JojoleproTestMod.Machines.TestMachine
{
	public class TestMachineDataSerializer : TagSerializer<TestMachineData, TagCompound>
	{
		public override TagCompound Serialize(TestMachineData value) => new TagCompound
		{
			["stuff"] = (int)value.stuff
		};

		public override TestMachineData Deserialize(TagCompound tag)
		{
			var stuff = tag.GetInt ("stuff");
			return new TestMachineData(stuff);
		}
	}
}

