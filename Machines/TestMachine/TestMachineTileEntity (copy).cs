﻿using System;
using Terraria;
using Terraria.Graphics.Effects;
using Terraria.Graphics.Shaders;
using Terraria.ID;
using Terraria.ModLoader;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using Terraria.UI;
using Terraria.DataStructures;
using Terraria.GameContent.UI;
using Terraria.ObjectData;
using Terraria.ModLoader.IO;

namespace JojoleproTestMod.Machines.TestMachine
{
	public class TestMachineTileEntityCP : ModTileEntity
	{
		//ID -> TestItem
		public static Dictionary<Point16,TestMachineData> instances = new Dictionary<Point16,TestMachineData>();
		//public static Dictionary<int,TestItem> instances = new Dictionary<int,TestItem>();

		//public static int s;
		//public TestItem item = new TestItem();

		public override bool ValidTile (int i, int j)
		{
			Tile tile = Main.tile [i, j];
			return tile.active () && tile.type == mod.TileType ("TestMachineTile") && tile.frameX == 0 && tile.frameY == 0;
		}
		public override void Update ()
		{
			//Main.NewText ("Wassup ma boi xddd "+((TestMachineTileEntity)TestMachineTileEntity.ByID[ID]).item.stuff);
		}
		public override void OnKill ()
		{
			TestMachineTileEntity.instances.Remove (Position);
		}

		public override int Hook_AfterPlacement (int i, int j, int type, int style, int direction)
		{
			var pos = new Point16 (i, j);
			if(!TestMachineTileEntity.instances.ContainsKey(pos))
				TestMachineTileEntity.instances.Add(pos,new TestMachineData());
			
			return Place (i, j);
		}
		public override void Load (TagCompound tag)
		{
			var pos = tag.Get<Point16> ("pos");
			var i = tag.Get<TestMachineData> ("data");
			if(!TestMachineTileEntity.instances.ContainsKey(pos))
				TestMachineTileEntity.instances.Add (pos, i);
		}
		public override TagCompound Save ()
		{
			return new TagCompound () {
				["pos"] = Position,
				["data"] = TestMachineTileEntity.instances[Position]
			};
		}
	}
}

