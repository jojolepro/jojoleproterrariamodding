﻿using System;
using Terraria;
using Terraria.Graphics.Effects;
using Terraria.Graphics.Shaders;
using Terraria.ID;
using Terraria.ModLoader;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using Terraria.UI;
using Terraria.DataStructures;
using Terraria.GameContent.UI;
using Terraria.ObjectData;
using Terraria.ModLoader.IO;

namespace JojoleproTestMod.Machines
{
	/// <summary>
	/// Data tile entity.
	/// You must provide a T data type which as at least a default constructor ()
	/// You must also define a Serializer for T and add this Serializer using
	///   TagSerializer.AddSerializer(new TSerializer());
	/// </summary>
	public class DataTileEntity<T> : ModTileEntity
	{
		//Position -> Data
		//if position changes, you need to move data to the new position
		public static Dictionary<Point16,T> instances = new Dictionary<Point16,T>();


		public override bool ValidTile (int i, int j)
		{
			Tile tile = Main.tile [i, j];
			return tile.active () && tile.type == mod.TileType (typeof(T).Name) && tile.frameX == 0 && tile.frameY == 0;
		}
		public override void OnKill ()
		{
			instances.Remove (Position);
		}

		public override int Hook_AfterPlacement (int i, int j, int type, int style, int direction)
		{
			var pos = new Point16 (i, j);
			if (!instances.ContainsKey (pos)) {
				instances.Add (pos, (T)Activator.CreateInstance(typeof(T)));
			}
			return Place (i, j);
		}
		public override void Load (TagCompound tag)
		{
			var pos = tag.Get<Point16> ("pos");
			var i = tag.Get<T> ("data");
			if(!instances.ContainsKey(pos))
				instances.Add (pos, i);
		}
		public override TagCompound Save ()
		{
			return new TagCompound () {
				["pos"] = Position,
				["data"] = instances[Position]
			};
		}
	}
}
