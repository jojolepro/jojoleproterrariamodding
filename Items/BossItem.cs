using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;
using Terraria.ID;

namespace JojoleproTestMod.Items
{
	public class BossItem : ModItem
	{
		public override void SetDefaults()
		{
			item.name = "Boss Item";
			item.toolTip = "Used to craft boss items";
			//item.toolTip2 = "Warning, test item :)";
			item.width = 150;
			item.melee = true;
			item.useStyle = 1;
			item.knockBack = 99;
			item.height = 150;
			item.maxStack = 1;
			item.rare = 3;
			item.damage = 9000;
			item.useTime = 2;
			item.useAnimation = 6;
			item.axe = 1000;
			item.pick = 10000;
			item.value = 10000;
			item.UseSound = SoundID.Item1;
			item.autoReuse = true;
			item.useTurn = true;
			item.tileBoost = 50;
		}

		public override void AddRecipes()
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.DirtBlock,1);
			recipe.AddTile(TileID.WorkBenches);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}

		/*public override void MeleeEffects(Player player, Rectangle hitbox)
		{
			int dust = Dust.NewDust(new Vector2(hitbox.X, hitbox.Y), hitbox.Width, hitbox.Height, mod.DustType("Sparkle"));
		}*/
	}
}