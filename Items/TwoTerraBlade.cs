﻿using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;
using Terraria.ID;
using Terraria.DataStructures;

namespace JojoleproTestMod.Items
{
	public class TwoTerraBlade : ModItem
	{
		public override void SetDefaults()
		{
			item.CloneDefaults(ItemID.TerraBlade);
			item.name = "Two Terra Blades";
			item.toolTip = "Fear the power of this sword...";
			item.melee = true;
			//item.knockBack = 5;
			item.rare = 10;
			item.damage = 200;
			item.useTime = 8;
			item.useAnimation = 8;
			item.value = 2000000;
			item.autoReuse = true;
			item.shootSpeed = 8;
		}

		public override void AddRecipes()
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.TerraBlade,1);
			recipe.AddIngredient (ItemID.LunarBar,40);
			recipe.AddIngredient (ItemID.FallenStar, 10);
			recipe.AddTile(TileID.LunarCraftingStation);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}

		/*public override void MeleeEffects(Player player, Rectangle hitbox)
		{
			int dust = Dust.NewDust(new Vector2(hitbox.X, hitbox.Y), hitbox.Width, hitbox.Height, mod.DustType("Sparkle"));
		}*/
		/*public override void OnHitNPC(Player player, NPC target, int damage, float knockback, bool crit)
		{
			target
		}*/
	}
}