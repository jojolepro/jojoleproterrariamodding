﻿﻿using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using JojoleproTestMod.Machines.TestMachine;
using System;

namespace JojoleproTestMod
{
	public class PollCommand : ModCommand
	{
		public override CommandType Type
		{
			get { return CommandType.World; }
		}

		public override string Command
		{
			get { return "poll"; }
		}

		public override string Usage
		{
			get { return "/poll"; }
		}

		public override string Description 
		{
			get { return "Polls the tile entities"; }
		}

		public override void Action(CommandCaller caller, string input, string[] args)
		{
			foreach (var v in TestMachineTileEntity.instances.Values) {
				Main.NewText (Convert.ToString(v));
			}
		}
	}
}