﻿using System;
using Terraria.DataStructures;
using System.Collections.Generic;

namespace JojoleproTestMod
{
	public class EnergyController
	{
		private static EnergyController instance = null;

		//id->Energy (tile entity)
		public Dictionary<int,Energy> energyTiles = new Dictionary<int,Energy>();
		private EnergyController ()
		{
			//replace Energy interface by extending ModTileEntity since we need to define custom OnPlace and OnRemove
			//ByID ref in TileEntity?
			//ByID [1];
		}
		public static EnergyController getInstance(){
			if (EnergyController.instance == null) {
				EnergyController.instance = new EnergyController ();
			}
			return EnergyController.instance;
		}
	}
}

