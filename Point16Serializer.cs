﻿using System.Collections;
using Terraria.ModLoader.IO;
using Terraria.DataStructures;

namespace JojoleproTestMod
{
	public class Point16Serializer : TagSerializer<Point16, TagCompound>
	{
		public override TagCompound Serialize(Point16 value) => new TagCompound
		{
			["x"] = (int)value.X,
			["y"] = (int)value.Y
		};

		public override Point16 Deserialize(TagCompound tag)
		{
			var x = tag.GetInt ("x");
			var y = tag.GetInt ("y");
			return new Point16(x,y);
		}
	}
}

