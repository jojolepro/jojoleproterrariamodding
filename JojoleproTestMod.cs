using System;
using Terraria;
using Terraria.Graphics.Effects;
using Terraria.Graphics.Shaders;
using Terraria.ID;
using Terraria.ModLoader;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using Terraria.UI;
using Terraria.DataStructures;
using Terraria.GameContent.UI;
using Terraria.ModLoader.IO;
using JojoleproTestMod.Machines.TestMachine;

namespace JojoleproTestMod
{
	public class JojoleproTestMod : Mod
	{
		public JojoleproTestMod()
		{
			Properties = new ModProperties()
			{
				Autoload = true,
				AutoloadGores = true,
				AutoloadSounds = true,
				AutoloadBackgrounds = true
			};

		}

		/*public override void ChatInput(string text, ref bool broadcast)
		{
			if (text[0] != '/')
			{
				return;
			}
			text = text.Substring(1);
			int index = text.IndexOf(' ');
			string command;
			string[] args;
			if (index < 0)
			{
				command = text;
				args = new string[0];
			}
			else
			{
				command = text.Substring(0, index);
				args = text.Substring(index + 1).Split(' ');
			}
			if (command == "thisisatest")
			{
				Test ();
				broadcast = false;
			}

		}*/
		public void Test()
		{
			Player player = Main.LocalPlayer;
			int type;
				for (int k = 0; k < Main.itemName.Length; k++)
				{
					if ("BossItem" == Main.itemName[k])
					{
						type = k;
						int stack = 1;
						player.QuickSpawnItem(type, stack);
						break;
					}
			}
		}
		public override void Load()
		{
			TagSerializer.AddSerializer(new TestItemSerializer());
			TagSerializer.AddSerializer(new Point16Serializer());
		}

		public override void Unload()
		{
		}
	}
}
